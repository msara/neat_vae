{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This program is free software: you can redistribute it and/or modify\n",
    "# it under the terms of the GNU General Public License as published by\n",
    "# the Free Software Foundation, either version 3 of the License, or\n",
    "# (at your option) any later version.\n",
    "#\n",
    "# This program is distributed in the hope that it will be useful,\n",
    "# but WITHOUT ANY WARRANTY; without even the implied warranty of\n",
    "# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n",
    "# GNU General Public License for more details.\n",
    "#\n",
    "# You should have received a copy of the GNU General Public License\n",
    "# along with this program.  If not, see <http://www.gnu.org/licenses/>.\n",
    "#\n",
    "# Copyright(C) 2019-2020 Max-Planck-Society\n",
    "#\n",
    "# The NEAT_VAE was developed at the Max-Planck-Institut fuer Astrophysik."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1. The NEAT_VAE model\n",
    "In our mathematical derivation, we first define a generative data model and afterwards build an inference process using information theoretical methods. The combination of both processes allows the resulting algorithm to perform in the opposite direction: It uses its input $d_i$ to infer a latent representation $z_i$ of the data, which is (among other factors like priors) led by the generative process aiming to regenerate the initial data vector $d_i$ from the latent space again. The minimization objective, or loss function, directing the algorithm's learning process is derived in the paper on *Bayesian decomposition of the Galactic multi-frequency sky using probabilistic autoencoders*. Here, we provide the implementation in Python and PyTorch.\n",
    "\n",
    "First, we will define the model class. This model is able to build its layers by itself, i.e., the layers are not hard coded. Based on the model's input parameters, it calculates the layer configurations and loops over the build layers to define the forward pass. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class NEAT_VAE(nn.Module):\n",
    "    def __init__(self, n_x, layers, neurons_layer, neurons_latent):\n",
    "        super (N_VAE, self).__init__()\n",
    "\n",
    "        num_enc_lay=int(layers/2-1)\n",
    "        num_dec_lay= num_enc_lay\n",
    "\n",
    "        self.encoder = nn.ModuleList([nn.Linear(n_x if k == 0 else neurons_layer, neurons_layer)\n",
    "        for k in range(num_enc_lay)])\n",
    "\n",
    "        self.mean = nn.Linear(neurons_layer, neurons_latent)\n",
    "        self.std = nn.Linear(neurons_layer, neurons_latent)\n",
    "\n",
    "        self.decoder = nn.ModuleList([nn.Linear(neurons_latent if k == 0 else neurons_layer, neurons_layer)\n",
    "        for k in range(num_dec_lay)])\n",
    "\n",
    "        n_y = n_x\n",
    "        self.out = nn.Linear(neurons_layer, n_y)\n",
    "\n",
    "    def encode (self,x):\n",
    "        e = x\n",
    "        for layer in self.encoder:\n",
    "            e = nn.functional.relu(layer(e))\n",
    "        return self.mean(e), self.std(e)\n",
    "\n",
    "    def decode (self,z):\n",
    "        d = z\n",
    "        for layer in self.decoder:\n",
    "            d = nn.functional.relu(layer(d))\n",
    "        return self.out(d)\n",
    "\n",
    "    def forward (self, x):\n",
    "        latent_mean, logvar = self.encode(x)\n",
    "        latent_std=torch.exp(0.5*logvar)\n",
    "        eps = torch.randn_like(latent_std)\n",
    "        z = latent_mean + eps*latent_std\n",
    "        output = self.decode(z)\n",
    "        latent_mean = latent_mean.to(device)\n",
    "        latent_std = latent_std.to(device)\n",
    "        z = z.to(device)\n",
    "        output = output.to(device)\n",
    "        return latent_mean, latent_std, z, output_vector"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "with input parameters\n",
    "- `n_x`: number of input channels $(k)$\n",
    "- `layers`: number of hidden layers\n",
    "- `neurons_layer`: number of hidden neurons per layer\n",
    "- `neurons_latent`: number of hidden neurons in the latent space $(l)$\n",
    "\n",
    "and outputs\n",
    "- `latent_mean`: latent space mean $\\mu_i$ (encoder output)\n",
    "- `latent_std`: latent space standard deviation, calculated from the latent space variance $\\Sigma_i$ (encoder output)\n",
    "- `z`: latent space variable\n",
    "- `output_vector`: reconstructed output vector $\\widetilde{d}_i$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 2. The loss function\n",
    "The loss function which has to be minimized is the Kullback-Leibler Divergence between the posterior distribution $\\mathcal{P}$ and the approximate distribution $Q$:\n",
    "\n",
    "\n",
    "$$\n",
    "D_{KL}[Q_{{\\Phi}}(\\cdot) \\mid\\mid P(\\cdot)] = \\frac{1}{2} \\sum_{i=1}^p \\Bigg[ - \\mathrm{tr} \\,(\\ln \\Sigma_i) - l \\left( 1+ \\mathrm{ln}\\,(2\\pi) \\right)+  \\frac{1}{p} \\, \\widehat{ \\xi_N}^2+ \\, \\mathrm{tr} \\, \\left( \\Sigma_i + {\\mu}_i {\\mu}_i^T \\right) + \\\\ \\mathrm{tr}\\, \\left( \\frac{1}{ t_{{\\psi}}(\\widehat{ \\xi_N})} \\left({d}_i - f_{\\widehat{{\\theta}}}({z}_i)\\right) \\left({d}_i - f_{\\widehat{{\\theta}}}({z}_i)\\right)^T \\right)  + \\mathrm{tr}\\,\\left(\\ln \\, t_{{\\psi}}(\\widehat{ \\xi_N})\\right) \\Bigg] + \\mathcal{H}_0 \n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "with\n",
    "- number of pixels $p$\n",
    "- latent space variance $\\Sigma_i$\n",
    "- number of latent space neurons $l$\n",
    "- latent noise parameter $\\widehat{\\xi_N}$\n",
    "- latent space mean $\\mu_i$\n",
    "- lognormal mapping $t_{\\psi}$ from $\\widehat{\\xi_N}$ to the noise covariance $N$ by $N = t_{\\psi}(\\widehat{\\xi_N}) = 1^{k \\times k} \\, \\, \\mathrm{exp} \\, (\\mu_N + \\sigma_N \\widehat{\\xi_N})$, where $\\mu_N$ and $\\sigma_N$ are fixed parameters \n",
    "- input data vector $d_i$\n",
    "- generative process  $f_{\\theta}$ mapping the latent space variable $z_i$ to $\\widetilde{d}_i$\n",
    "- latent space vector $z_i$\n",
    "- a constant $\\mathcal{H}_0$.\n",
    "\n",
    "We implemented this loss function as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def loss_NVAE(latent_mean, latent_std, xi_N, mu, sigma, input_vector, \n",
    "              output_vector, batchsize, num_of_pixels):\n",
    "    t = torch.exp(mu + sigma*xi_N)\n",
    "    A = (torch.sum(torch.log(latent_std**2)))/batchsize \n",
    "        + neurons_latent*(1+torch.log(torch.as_tensor(2*math.pi)))\n",
    "    B = (torch.sum(xi_N**2))*n_x/num_of_pixels\n",
    "    C = (torch.sum(latent_std**2 + latent_mean**2))/batchsize\n",
    "    D = (torch.sum(1/t * (input_vector - output_vector)**2))/batchsize\n",
    "    E = (torch.sum(torch.log(t)))*n_x\n",
    "    loss = 0.5*(-A + B + C + D + E)\n",
    "    loss = loss.to(device)\n",
    "    return loss"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The variables used in the code have the following definitions (except the variables already defined at the `NEAT_VAE` model):\n",
    "\n",
    "- `xi_N`: latent noise parameter $\\widehat{\\xi_N}$\n",
    "- `mu`, `sigma`: fixed parameters for lognormal mapping ( $\\mu_N$ and $\\sigma_N$)\n",
    "- `input_vector`: input data vector $d_i$\n",
    "- `batchsize`: batch size used in training, in our case 128\n",
    "- `num_of_pixels`: number of pixels $p$\n",
    "\n",
    "In the loss function, we had to correct for some volume factors introduced by our implementation, which we explain in the following. In theory, the latent noise parameter $\\widehat{\\xi_N}$ is a `n_x` = 35-dimensional vector with one value for all dimensions, and $t_{\\psi}(\\widehat{\\xi_N})$ is a `n_x` $\\cdot$ `n_x`-dimensional matrix with equal entries on the diagonal. In our implementation, **torch.sum** represents the trace function and $\\widehat{\\xi_N}$ and $t_{\\psi}(\\widehat{ \\xi_N})$ are scalars. Therefore, we have to multiplicate terms B and E with a factor of `n_x` to ensure the correct computation of the trace. In term D this problem is not occuring, since $t_{\\psi}(\\widehat{ \\xi_N})$ gets broadcasted with the input and output vectors, thus it is automatically converted into the appropriate dimension. Terms A, C and D have to be divided by the batch size, since they contain quantities which are computed using minibatching. In minibatching, the algorithm takes a sample of the training data to compute the loss and perform weight updating. The sample size equals the chosen batchsize, in our case 128. This means each quantity that depends on the batched input datavector is computed 128 times in one epoch. The other quantities, like $\\widehat{ \\xi_N}$ and $t_{\\psi}(\\widehat{ \\xi_N})$, are not minibatched (since they do not depend on the input datavector) and are thus only calculated once in each epoch. To correct the dimensions, we therefore divide terms A, C and D by the batchsize. "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
